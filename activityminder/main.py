import argparse
from datetime import datetime, timedelta, timezone
import json
import logging
import time
from typing import List, NoReturn

import dateutil.parser
import requests


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter("{message}", style="{")
ch.setFormatter(formatter)
logger.addHandler(ch)
logger.propagate = False


class Bee:
    def __init__(self, api_endpoint: str, api_token: str, whitelist: List[str]):
        """
        delay : int
            The number of seconds to sleep and how far back to get data for.
            This is used to send consistent chunks of data.
        whitelist : str
            Comma delimited list of terms to look for in window titles.
            If any of the terms are found in a window title, it is considered "productive".
            This is required due to the lack of categorization in ActivityWatch at this time.
        """
        self.whitelist: List[str] = whitelist
        self.api_endpoint = api_endpoint
        self.api_token = api_token

    def get_nonafk_events(self) -> List[dict]:
        """
        Get all nonafk events from a wide timeframe (10 days)

        Timeframe is wide since I haven't figured out how to make very time specific
        queries, e.g. within the span 10 minutes.
        """

        self.now: datetime = datetime.now(timezone.utc)
        self.ago: datetime = self.get_ago()

        # TODO figure out why this doesn't work
        # ago = now - timedelta(seconds=self.delay)
        ago = self.now - timedelta(days=5)

        headers = {"Content-type": "application/json", "charset": "utf-8"}
        query = """afk_events = query_bucket(find_bucket('aw-watcher-afk_'));
window_events = query_bucket(find_bucket('aw-watcher-window_'));
RETURN = filter_period_intersect(window_events, filter_keyvals(afk_events, 'status', ['not-afk']));""".split(
            "\n"
        )
        data = {
            "timeperiods": ["/".join([ago.isoformat(), self.now.isoformat()])],
            "query": query,
        }
        r = requests.post(
            "http://localhost:5600/api/0/query/",
            data=bytes(json.dumps(data), "utf-8"),
            headers=headers,
            params={},
        )
        return json.loads(r.text)[0]

    def get_ago(self) -> datetime:
        r = requests.get(self.api_endpoint, data={"auth_token": self.api_token})
        datapoints = json.loads(r.text)

        return datetime.fromtimestamp(datapoints[0]["timestamp"], timezone.utc)

    def get_prod_secs(self) -> int:
        """
        Get total seconds since last `delay` seconds considered "productive"
        """
        nonafk_events = self.get_nonafk_events()

        durations = []

        for event in nonafk_events:
            duration = event["duration"]
            timestamp = dateutil.parser.parse(event["timestamp"])
            title = event["data"]["title"]

            if self.is_productive(title) and self._is_recent(timestamp, duration):
                # truncate durations of older, longer chunks of time
                if timestamp < self.ago:
                    duration -= (self.ago - timestamp).total_seconds()
                durations.append(duration)

        return sum(durations)

    def _is_recent(self, timestamp: datetime, duration: int) -> bool:
        """
        Check if timestamp and duration are within the timeframe given by `delay`.
        """
        return self.ago <= timestamp + timedelta(seconds=duration) <= self.now

    def is_productive(self, title: str) -> bool:
        """
        Check if any whitelist term is in the given window title.
        """
        return any(whiteterm.lower() in title.lower() for whiteterm in self.whitelist)

    def report_loop(self) -> NoReturn:
        """
        Regularly post datapoints to Beeminder based on filtered ActivityWatch data
        """
        while True:
            secs: int = self.get_prod_secs()
            hours: float = secs / 60 / 60

            human_time: str = str(timedelta(seconds=secs))
            logger.info(human_time)

            if secs > 0:
                r = requests.post(
                    self.api_endpoint,
                    data={
                        "auth_token": self.api_token,
                        "comment": "sent from activityminder",
                        "value": hours,
                    },
                )
                r.raise_for_status()

            time.sleep(600)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("api_endpoint_url")
    parser.add_argument("api_token")
    parser.add_argument("whitelist", help="str of comma delimited whitelist terms")
    args = parser.parse_args()

    whitelist = args.whitelist.split(",")

    bee = Bee(args.api_endpoint_url, args.api_token, whitelist)
    bee.report_loop()
